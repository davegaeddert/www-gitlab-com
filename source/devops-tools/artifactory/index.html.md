---
layout: markdown_page
title: "JFrog Artifactory"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
JFrog Artifactory is a tool designed to store the binary output of the build process for use in distribution and deployment.  Artifactory provides support for a number of package formats such as Maven, Debian, NPM, Helm, Ruby, Python, and Docker. JFrog offers high availability, replication, disaster recovery, scalability, and works with many on-prem and cloud storage offerings.

GitLab also offers high availability and replication, and scalability, also available using on-prem or cloud storage, but at the moment offers less package type compatibility than Artifactory does (Maven, Docker, NPM). However, GitLab also offers functionality to automate the entire DevOps life cycle from planning, to creation, build, verify, security testing, deploying, and monitoring. The built-in binary package repositories that GitLab offers are just the tip of the ice-berg.

## Resources
* [Artifactory website](https://jfrog.com/artifactory/)
* [Wikipedia page on binary repository managers](https://en.wikipedia.org/wiki/Binary_repository_manager)

## Pricing
* Base server subscription cost is $3k/year (Pro), Pro X adds X-Ray add-on product, training, and better support for $14.4k/year. Enterprise supports multi-site with up to 3 servers, advanced storage (including cloud), and central control (Mission Control) add-on for about $30k/year. Their newest offering is Enterprise+ which is considered "platform" purchase, in that you are getting almost everything JFrog offers (all Artifactory add-ons), for 6 servers. This we've heard runs about $124k/year.
* [Pricing page](https://jfrog.com/artifactory/buy-now/#pro)
* Heard from procurement at a prospect:
   > "Enterprise+ was $95,000 in December 2018 and got increased by 20% in January 2019, so $124,000. That's for a 6 server licence"
   > "sales guy told them to buy at list or walk away"

## Comparison

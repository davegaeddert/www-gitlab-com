---
layout: markdown_page
title: Product Direction - Fulfillment
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product direction for GitLab's Fulfillment group. Below are the categories Fulfillment currently owns and improves.

If you'd like to discuss this vision directly with the Product Manager for Fulfillment,
feel free to reach out to Mike Karampalas via [e-mail](mailto:mkarampalas@gitlab.com),
[Twitter](https://twitter.com/mkarampalas), or by [scheduling a video call](https://calendly.com/mkarampalas).

## ⛵️ Overview

Engaging in business transactions with GitLab should be minimally invasive and when noticed, should be a positive, empowering and user-friendly experience. Our goal is to fulfill the needs of users all around the world in a manner that does not exclude them through a lack of flexibility in payment methods, currencies or billing models.

In addition to our customers, we also prioritise the experience of GitLab Team Members (and Resellers). In order for our GitLab Team Members and Resellers to provide a great service to our customers, the administration and 'behind-the-scenes' user experience of our billing and licensing services are held to the same standard as the rest of our product.

Due to the fact that the Fulfillment group is responsible for collecting and storing personal and billing information, we also need to ensure that GitLab's services meet all compliance, security and privacy requirements.

<!-- ### ⭐️ North Stars


Please click into the epics to see what's next and why. -->
 
## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/#prioritization). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Fulfillment can be viewed [on the issue board](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Afulfillment), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!

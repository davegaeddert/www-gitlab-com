---
layout: handbook-page-toc
title: Ecosystem Team
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

Support the seamless integration between GitLab and 3rd party products and services, expanding GitLab's market opportunities by empowering developers to contribute.

## Mission

The Ecosystem team is responsible for designing, building, and maintaining:
* Guiding the overall design of the GitLab API
* Expanding the GitLab API with new functionality and data availabilty
* Integrations between GitLab and key external products
* Documentation, instructions, and best practice guides for how to work with GitLab APIs
* Best practices for external contributors looking to develop their own integrations in the GitLab codebase

## Team Members

The following people are permanent members of the Ecosystem Team:

<%= direct_team(manager_role: 'Engineering Manager, Ecosystem') %>

## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Ecosystem/, direct_manager_role: 'Engineering Manager, Ecosystem') %>

## Meetings

Whenever possible, we prefer to communicate asynchronously using issues, merge requests, and Slack. However, face-to-face meetings are useful to establish personal connection and to address items that would be more efficiently discussed synchronously such as blockers.
  * Weekly Ecosystem product and engineering meeting - Wednesdays 11:00am CST/16:00 UTC
    * Note that the weekly meeting schedule will undergo some iteration as new team members come on board. Meetings will be recorded and everyone is encouraged to watch the meeting on our [GitLab Unfiltered playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KpoFo2QyceT_4CNfcq7do4s) if they are not able to attend.
  * Async Standups - Daily in Slack [#g_ecosystem](https://gitlab.slack.com/messages/CK4Q4709G)
    * We use the [Geekbot Slack plugin](https://geekbot.com/dashboard/) to prompt team members for async updates. The intention of these updates is to provide visibility and to bring up anything slowing or blocking progress. There is a weekly social question on Monday for team members to share any highlights from the weekend.
      * Prompts:
        * Highlight something you did today.
        * Anything slowing/blocking your progress?
        * Tell us about something fun or interesting you did outside of work last week/weekend. (Mondays only)

## Workflow

### Issue boards

We use the following issue boards to prioritize and track our work.

  * [Ecosystem Planning Board](https://gitlab.com/groups/gitlab-org/-/boards/1167634) - For scheduling and prioritization. It provides an overview of the backlog and planned issues by milestone.
  * [Ecosystem Workflow Board](https://gitlab.com/groups/gitlab-org/-/boards/1290820) - Tracks the progress of issues that are scheduled for the current milestone.
  * [Ecosystem Team Member Board](https://gitlab.com/groups/gitlab-org/-/boards/1365136) - Tracks ~group::ecosystem labeled issues by assigned team member.

### Issue Weights

As a brand new team with engineers still onboarding, we are still discussing and experimenting with the best way to handle estimation/weighting of issues. This ongoing discussion can be found [here](https://gitlab.com/gitlab-org/ecosystem-team/team-tasks/issues/2). We have adopted the weighting system used by other GitLab teams such as Memory, Geo, and Plan:Project Management. From the 12.4 milestone to 12.6, engineers will assign weights after an issue is completed to help us establish a baseline. After that we will assign weights to backlog items in order to assist with scheduling work.

| Weight | Description  |
| --- | --- | 
| 1: Trivial | The problem is very well understood, no extra investigation is required, the exact solution is already known and just needs to be implemented, no surprises are expected, and no coordination with other teams or people is required.<br><br>Examples are documentation updates, simple regressions, and other bugs that have already been investigated and discussed and can be fixed with a few lines of code, or technical debt that we know exactly how to address, but just haven't found time for yet. |
| 2: Small | The problem is well understood and a solution is outlined, but a little bit of extra investigation will probably still be required to realize the solution. Few surprises are expected, if any, and no coordination with other teams or people is required.<br><br>Examples are simple features, like a new API endpoint to expose existing data or functionality, or regular bugs or performance issues where some investigation has already taken place. |
| 3: Medium | Features that are well understood and relatively straightforward. A solution will be outlined, and most edge cases will be considered, but some extra investigation will be required to realize the solution. Some surprises are expected, and coordination with other teams or people may be required.<br><br>Bugs that are relatively poorly understood and may not yet have a suggested solution. Significant investigation will definitely be required, but the expectation is that once the problem is found, a solution should be relatively straightforward.<br><br>Examples are regular features, potentially with a backend and frontend component, or most bugs or performance issues. |
| 5: Large | Features that are well understood, but known to be hard. A solution will be outlined, and major edge cases will be considered, but extra investigation will definitely be required to realize the solution. Many surprises are expected, and coordination with other teams or people is likely required.<br><br>Bugs that are very poorly understood, and will not have a suggested solution. Significant investigation will be required, and once the problem is found, a solution may not be straightforward.<br><br>Examples are large features with a backend and frontend component, or bugs or performance issues that have seen some initial investigation but have not yet been reproduced or otherwise "figured out". |

Anything larger than 5 should be broken down.

## Common Links

  * [Ecosystem Epics](https://gitlab.com/groups/gitlab-org/-/epics/1515) - A primary collection point for grouped collections of integrations.
  * [Ecosystem Subgroup](https://gitlab.com/gitlab-org/ecosystem-team) - Issues and templates related to team process.
  * [Ecosystem Stage Strategy](/direction/ecosystem/)
  * [Product Development Timeline](/handbook/engineering/workflow/#product-development-timeline)
  * Slack channel: [#g_ecosystem](https://gitlab.slack.com/messages/CK4Q4709G)
